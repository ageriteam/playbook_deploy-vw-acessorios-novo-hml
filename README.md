## Playbook para deploy de site PHP em container

### Extra Vars obrigatórias

#### "{{ APP_DIR }}"
* Informar qual será o diretório de deploy da aplicação onde será construido os diretórios
    * shared
    * releases
    * current

#### "{{ GIT_REPO }}"
* Repositório git do projeto

#### "{{ GIT_BRANCH }}"
* Branch default onde será feito deploy. ex: master

#### Diferenciais da playbook
* A Playbook copia 4 arquivos para dentro do projeto, sendo eles:
    * **cron-tasks**, contendo o agendamento da execução dos scripts. O arquivo é copiado para /etc/cron.d/ e o serviço do cron é iniciado através do comando `cron` na subida do container
    * **integrator.sh**, script para rodar de 1 em 1 minuto.
    * **october.sh**, script para rodar na subida do container
    * **processleads.sh**, script para rodar de 1 em 1h
* O log de todos os .sh estão dentro do container em /var/log