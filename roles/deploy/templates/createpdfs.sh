#!/bin/bash

DATE=`date +%d/%m/%Y" "%H:%M:%S`

/usr/local/bin/php /var/www/html/artisan vw:createpdfs 2>&1 | tee -a /var/log/createpdfs-output.log
echo "${DATE} - vw:createpdfs executado." >> /var/log/createpdfs.log
exit 0