#!/bin/bash

DATE=`date +%d/%m/%Y" "%H:%M:%S`

/usr/local/bin/php /var/www/html/artisan vw:integrator 2>&1 | tee -a /var/log/integrator-output.log
echo "${DATE} - vw:integrator executado." >> /var/log/integrator.log
exit 0