#!/bin/bash

DATE=`date +%d/%m/%Y" "%H:%M:%S` 

/usr/local/bin/php /var/www/html/artisan vw:processleads 2>&1 | tee -a /var/log/processleads-output.log
echo "${DATE} - vw:processleads executado." >> /var/log/processleads.log
exit 0